<?php
/**
 * @package    	Ktzagcplugin
 * @subpackage 	Ktzagcplugin/admin
 * @author     	Gian Mokhammad Ramadhan <g14nblog@gmail.com>
 * @since 		1.1.5
 */

class Ktzagcplugin_Sidebar_Box {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.4.0
	 * @access   private
	 * @var      string    $ktzagcplugin    The ID of this plugin.
	 */
	private $ktzagcplugin;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.4.0
	 * @var      string    $ktzagcplugin    The name of this plugin.
	 */
	public function __construct( $ktzagcplugin ) {

		$this->ktzagcplugin = $ktzagcplugin;

	}

	/**
	 * Register the meta boxes on options page.
	 *
	 * @since    1.4.0
	 */
	public function add_meta_boxes() {

		add_meta_box(
				'sidebar_box',							// Meta box ID
				__( 'Information', $this->ktzagcplugin ), 	// Meta box Title
				array( $this, 'render_meta_box' ),			// Callback defining the plugin's innards
				'ktzagcplugin_settings_side',						// Screen to which to add the meta box
				'side'									// Context
				);

	}

	/**
	 * Print the meta box on options page.
	 *
	 * @since     1.4.0
	 */
	public function render_meta_box( $active_tab ) {

		require( plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/ktzagcplugin-sidebar-display.php' );

	} // end render_meta_box

} //end Ktzagcplugin_Sidebar_Box
