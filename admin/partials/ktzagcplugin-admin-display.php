<?php
/**
 * Provide a dashboard view for the plugin
 *
 * This file is used to markup the plugin settings page.
 *
 * @link       http://google.com
 * @since      1.0.0
 *
 * @package    koponk
 * @subpackage koponk/admin/partials
 */

/**
 * Options Page
 *
 * Renders the settings page contents.
 *
 * @since       1.0.0
*/

?>

<div class="wrap">
	<?php settings_errors( $this->ktzagcplugin . '-notices' ); ?>

	<h2 class="nav-tab-wrapper">
		<?php
		foreach( $tabs as $tab_slug => $tab_name ) {

			$tab_url = add_query_arg( array(
				'settings-updated' => false,
				'tab' => $tab_slug
				) );

			$active = $active_tab == $tab_slug ? ' nav-tab-active' : '';

			echo '<a href="' . esc_url( $tab_url ) . '" title="' . esc_attr( $tab_name ) . '" class="nav-tab' . $active . '">';
			echo esc_html( $tab_name );
			echo '</a>';
		}
		?>
	</h2>

	<div id="poststuff">

		<div id="post-body" class="metabox-holder columns-2">
		
			<div class="postbox-container columns-2">

				<div id="postbox-container-2" class="postbox-container">
						
						<?php do_meta_boxes( 'ktzagcplugin_settings_' . $active_tab, 'normal', $active_tab ); ?>

				</div><!-- .postbox-container-2-->

			</div><!-- .postbox-container.columns-2-->
			
			<div id="postbox-container-1" class="postbox-container">

				<?php do_meta_boxes( 'ktzagcplugin_settings_side', 'side', $active_tab ); ?>

			</div><!-- .postbox-container-1-->

		</div><!-- #post-body-->

	</div><!-- #poststuff-->
</div><!-- .wrap -->
