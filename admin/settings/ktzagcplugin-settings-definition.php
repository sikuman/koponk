<?php
/**
 *
 *
 * @link       http://google.com
 * @since      1.0.0
 *
 * @package    koponk
 * @subpackage koponk/admin
 */
 
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * [get_default_tab_slug description]
 * @return [type] [description]
 */
function ktzagcplugin_get_default_tab_slug() {

	return key( ktzagcplugin_get_tabs() );
}

/**
 * Retrieve settings tabs
 *
 * @since    1.0.0
 * @return    array    $tabs    Settings tabs
 */
function ktzagcplugin_get_tabs() {
	
	$settings = ktzagcplugin_get_settings();

	$tabs                = array();
	$tabs['default_tab'] = __( 'General Settings', 'ktzagcplugin' );
	if( ! empty( $settings['addons'] ) ) {
		$tabs['addons']  = __( 'Addon', 'ktzagcplugin' );
	}
	return apply_filters( 'ktzagcplugin_settings_tabs', $tabs );
}

/**
 * 'Whitelisted' Ktzagcplugin settings, filters are provided for each settings
 * section to allow extensions and other plugins to add their own settings
 *
 *
 * @since    1.0.0
 * @return    mixed    $value    Value saved / $default if key if not exist
 */
function ktzagcplugin_get_settings() {
	
	$settings = array(
	
		'default_tab' => apply_filters( 'ktzplg_settings_general_filter', 
			array(
			
				/*
				sangat berat dan akan terposting otomatis tanpa ada filter termasuk ping, cape deh... 
				Ane nonaktifkan saja, tolong jangan diaktifkan fitur ini lagi gaes.
			
				'ktzplg_autopost_agccontent'                   => array(
					'id' => 'ktzplg_autopost_agccontent',
					'name' => __( 'Active auto post', 'ktzagcplugin' ),
					'desc' => __( 'If you want auto post agc results, please check this option, leave blank if you want only AGC. If you check this option, every visitor visit your single AGC, title and content will automatic save in your database. Make sure you have a lot of space for this. :D', 'ktzagcplugin' ),
					'type' => 'checkbox'
				),
				'ktzplg_autopost_category'                => array(
					'id' => 'ktzplg_autopost_category',
					'name' => __( 'Auto post (Category)', 'ktzagcplugin' ),
					'desc' => __( 'If you activated auto post featured, please select this category, all auto post will place in this category.', 'ktzagcplugin' ),
					'std'  => __( '1', 'ktzagcplugin' ),
					'type' => 'category_select'
				),
				'ktzplg_autopost_status'                      => array(
					'id' => 'ktzplg_autopost_status',
					'name'    => __( 'Auto post (Status)', 'ktzagcplugin' ),
					'desc'    => __( 'If you activated auto post featured, please select this post status, all auto post will place in this status.', 'ktzagcplugin' ),
					'options' => array(
						'draft'   => __( "Draft", 'ktzagcplugin' ),
						'publish' => __( "Publish", 'ktzagcplugin' )
					),
					'std'  => __( 'publish', 'ktzagcplugin' ),
					'type'    => 'select'
				),
				*/

				'general_tab_settings'                => array(
					'id'	=> 'general_tab_settings',
					'name' => '<strong>' . __( 'General', 'ktzagcplugin' ) . '</strong>',
					'type' => 'header'
				),
				'ktzplg_badword'              => array(
					'id' => 'ktzplg_badword',
					'name' => __( 'Badword', 'ktzagcplugin' ),
					'std'  => __( '', 'ktzagcplugin' ),
					'type' => 'textarea'
				),
				'ktzplg_rand_keyword'              => array(
					'id' => 'ktzplg_rand_keyword',
					'name' => __( 'Keyword', 'ktzagcplugin' ),
					'type' => 'textarea'
				),
				'ktzplg_nofollow_head'                => array(
					'id' => 'ktzplg_nofollow_head',
					'name' => __( 'Stop Index To Search Engine', 'ktzagcplugin' ),
					'type' => 'checkbox'
				),
				'ktzplg_agc_content'                => array(
					'id' => 'ktzplg_agc_content',
					'name' => __( 'AGC content(Single)', 'ktzagcplugin' ),
					'desc' => __( '
					<br>[ktzagcplugin_image]
					<br>[ktzagcplugin_text] G
					<br>[ktzagcplugin_text_ask]
					<br>[ktzagcplugin_text_googlenewrss] grss
					<br>[ktzagcplugin_text_yahoo]
					<br>[ktzagcplugin_text_bing] 1-5
					<br>[ktzagcplugin_image_google] G
					<br>[ktzagcplugin_image_yahoo]
					<br>[ktzagcplugin_video]
					<br>[ktzagcplugin_google_sugestion]
					<br>[ktzagcplugin_google_sugestion_nolink]
					<br>[max_keyword="" number="4" related="true"]'
					, 'ktzagcplugin' ),
					'std'  => __( '[ktzagcplugin_image]<br />[ktzagcplugin_text number="4"]<br />[ktzagcplugin_video]<br />[ktzagcplugin_text source="ask" number="4"]<br />[ktzagcplugin_text source="bing" number="4" related="true"]', 'ktzagcplugin' ),
					'type' => 'rich_editor'
				),
				'ktzplg_agc_searchresults'                      => array(
					'id' => 'ktzplg_agc_searchresults',
					'name'    => __( 'AGC in Search Results', 'ktzagcplugin' ),
					'options' => array(
						'disable'   => __( "Disable", 'ktzagcplugin' ),
						'google' => __( "Search Results Generate From <b>Google</b>", 'ktzagcplugin' ),
						'bing' => __( "Search Results Generate From <b>Bing</b>", 'ktzagcplugin' ),
						'yahoo' => __( "Search Results Generate From <b>Yahoo</b>", 'ktzagcplugin' ),
						'ask'     => __( "Search Results Generate From <b>Ask</b>", 'ktzagcplugin' )
					),
					'std'    => 'google',
					'type'    => 'radio'
				),
				'proxy_tab_settings'                => array(
					'id'	=> 'proxy_tab_settings',
					'name' => '<strong>' . __( 'Proxy Settings', 'ktzagcplugin' ) . '</strong>',
					'type' => 'header'
				),
				'ktzplg_curl_proxy'                => array(
					'id' => 'ktzplg_curl_proxy',
					'name' => __( 'Proxy', 'ktzagcplugin' ),
					'desc' => __( 'With some reason maybe you need proxy for grab AGC, you can fill here with format: IPproxy:port for example: 127.0.0.0:80', 'ktzagcplugin' ),
					'type' => 'text'
				),
				'ktzplg_curl_proxy_userpass'                => array(
					'id' => 'ktzplg_curl_proxy_userpass',
					'name' => __( 'Proxy Username And Password', 'ktzagcplugin' ),
					'desc' => __( 'If you use premium proxy maybe you need username and password for auth your proxy: Username:password for example: user:pass', 'ktzagcplugin' ),
					'type' => 'text'
				),
			)
		),
		
		
		'addons'  => apply_filters( 'ktzplg_settings_addons_filter', array() ),
		
	);
	
	return $settings;
}
