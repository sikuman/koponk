<?php

/**
 * Fired during plugin activation
 *
 * @link       http://google.com
 * @since      1.0.0
 *
 * @package    koponk
 * @subpackage koponk/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    koponk
 * @subpackage koponk/includes
 * @author     sikuman
 */
class Ktzagcplugin_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		# flash rewrite rule
		ktzplg_flush_rewrite_rules();
		
		#add option to sql
		/*
		sangat berat dan akan terposting otomatis tanpa ada filter termasuk ping, cape deh... 
		Ane nonaktifkan saja, tolong jangan diaktifkan fitur ini lagi gaes.
		add_option('ktzplg_autopost_agccontent', '');
		add_option('ktzplg_autopost_category', '1');
		add_option('ktzplg_autopost_status', __('publish', 'ktzagcplugin' ) );
		*/
		add_option('ktzplg_badword', '');
		add_option('ktzplg_rand_keyword', '' );
		add_option('ktzplg_agc_content', '[ktzagcplugin_image]<br />[ktzagcplugin_text number="4"]<br />[ktzagcplugin_video]<br />[ktzagcplugin_text source="ask" number="4"]<br />[ktzagcplugin_text source="bing" number="4" related="true"]');
		add_option('ktzplg_agc_searchresults', 'google');
		add_option('ktzplg_curl_proxy', '');
		add_option('ktzplg_curl_proxy_userpass', '');
	}

}