<?php
/**
 * The file that defines all  necessery shortcode for scrapper
 *
 * @link       http://google.com
 * @since      1.0.0
 *
 * @package    koponk
 * @subpackage koponk/modules
 */

function ktzagcplugin_random_term_shorcode( $atts ) {
	extract( shortcode_atts( array(
	 'number' => 10
	), $atts ) );
	$numbers = empty($number) ? 10 : $number;
	
	return ktzplg_get_random_term( $numbers );
}
add_shortcode( 'ktzagcplugin_random_term', 'ktzagcplugin_random_term_shorcode' );

function ktzagcplugin_google_trend_shorcode( $atts ) {
	extract( shortcode_atts( array(
	 'country_code' => 'p1',
	 'number' => 10
	), $atts ) );
	$numbers = empty($number) ? 10 : $number;
	$country_codes = empty($country_code) ? 10 : $country_code;
	
	return ktzplg_get_google_trend( $country_codes, $numbers );
}
add_shortcode( 'ktzagcplugin_google_trend', 'ktzagcplugin_google_trend_shorcode' );

/* 
 * AGC Text Shortcode 
 * @return do_action funct, keyword, numb_sentence
 * support source: google, bing and ask
 */
function ktzagcplugin_text_shortcode( $atts ) {
	extract( shortcode_atts( array(
      'keyword' => '', # keyword
      'max_keyword' => '', # Maximal keyword
      'source' => 'google', # source AGC
      'number' => '',  #number show agc
	  'related' => '' #use related by keyword - true/false
      ), $atts ) );	
	if ( $keyword != '' ) {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	$sources = empty($source) ? 'google' : $source;
	$numbers = empty($number) ? 4 : $number;
	$relateds = empty($related) ? false : $related;
	if ( $sources == 'google' ) {
		return ktzplg_get_google_article( $keywords, $max_keywords, $numbers, $relateds );
	}
}
add_shortcode('ktzagcplugin_text', 'ktzagcplugin_text_shortcode'); /* format [ktzagcplugin_text keyword="" source="" number=""] */
/*
tambahan neng kene
*/

function ktzagcplugin_text_bing_shortcode( $atts ) {
	extract( shortcode_atts( array(
      'keyword' => '', # keyword
      'max_keyword' => '', # Maximal keyword
      'source' => 'bing', # source AGC
      'number' => '',  #number show agc
	  'related' => '' #use related by keyword - true/false
      ), $atts ) );	
	if ( $keyword != '' ) {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	$sources = empty($source) ? 'bing' : $source;
	$numbers = empty($number) ? 4 : $number;
	$relateds = empty($related) ? false : $related;
	if ( $sources == 'bing' ) {
		return ktzplg_get_bing_article( $keywords, $max_keywords, $numbers, $relateds );
	}
}
add_shortcode('ktzagcplugin_text_bing', 'ktzagcplugin_text_bing_shortcode'); /* format [ktzagcplugin_text keyword="" source="" number=""] */


function ktzagcplugin_text_bing_2_shortcode( $atts ) {
	extract( shortcode_atts( array(
      'keyword' => '', # keyword
      'max_keyword' => '', # Maximal keyword
      'source' => 'bing', # source AGC
      'number' => '',  #number show agc
	  'related' => '' #use related by keyword - true/false
      ), $atts ) );	
	if ( $keyword != '' ) {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	$sources = empty($source) ? 'bing' : $source;
	$numbers = empty($number) ? 4 : $number;
	$relateds = empty($related) ? false : $related;
	if ( $sources == 'bing' ) {
		return ktzplg_get_bing_article_2( $keywords, $max_keywords, $numbers, $relateds );
	}
}
add_shortcode('ktzagcplugin_text_bing_2', 'ktzagcplugin_text_bing_2_shortcode');

function ktzagcplugin_text_bing_3_shortcode( $atts ) {
	extract( shortcode_atts( array(
      'keyword' => '', # keyword
      'max_keyword' => '', # Maximal keyword
      'source' => 'bing', # source AGC
      'number' => '',  #number show agc
	  'related' => '' #use related by keyword - true/false
      ), $atts ) );	
	if ( $keyword != '' ) {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	$sources = empty($source) ? 'bing' : $source;
	$numbers = empty($number) ? 4 : $number;
	$relateds = empty($related) ? false : $related;
	if ( $sources == 'bing' ) {
		return ktzplg_get_bing_article_3( $keywords, $max_keywords, $numbers, $relateds );
	}
}
add_shortcode('ktzagcplugin_text_bing_3', 'ktzagcplugin_text_bing_3_shortcode');
function ktzagcplugin_text_bing_4_shortcode( $atts ) {
	extract( shortcode_atts( array(
      'keyword' => '', # keyword
      'max_keyword' => '', # Maximal keyword
      'source' => 'bing', # source AGC
      'number' => '',  #number show agc
	  'related' => '' #use related by keyword - true/false
      ), $atts ) );	
	if ( $keyword != '' ) {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	$sources = empty($source) ? 'bing' : $source;
	$numbers = empty($number) ? 4 : $number;
	$relateds = empty($related) ? false : $related;
	if ( $sources == 'bing' ) {
		return ktzplg_get_bing_article_4( $keywords, $max_keywords, $numbers, $relateds );
	}
}
add_shortcode('ktzagcplugin_text_bing_4', 'ktzagcplugin_text_bing_4_shortcode');

function ktzagcplugin_text_bing_5_shortcode( $atts ) {
	extract( shortcode_atts( array(
      'keyword' => '', # keyword
      'max_keyword' => '', # Maximal keyword
      'source' => 'bing', # source AGC
      'number' => '',  #number show agc
	  'related' => '' #use related by keyword - true/false
      ), $atts ) );	
	if ( $keyword != '' ) {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	$sources = empty($source) ? 'bing' : $source;
	$numbers = empty($number) ? 4 : $number;
	$relateds = empty($related) ? false : $related;
	if ( $sources == 'bing' ) {
		return ktzplg_get_bing_article_5( $keywords, $max_keywords, $numbers, $relateds );
	}
}
add_shortcode('ktzagcplugin_text_bing_5', 'ktzagcplugin_text_bing_5_shortcode');
/*
bingrss
*/
function ktzagcplugin_text_ask_shortcode( $atts ) {
	extract( shortcode_atts( array(
      'keyword' => '', # keyword
      'max_keyword' => '', # Maximal keyword
      'source' => 'ask', # source AGC
      'number' => '',  #number show agc
	  'related' => '' #use related by keyword - true/false
      ), $atts ) );	
	if ( $keyword != '' ) {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	$sources = empty($source) ? 'ask' : $source;
	$numbers = empty($number) ? 4 : $number;
	$relateds = empty($related) ? false : $related;
	if ( $sources == 'ask' ) {
		return ktzplg_get_ask_article( $keywords, $max_keywords, $numbers, $relateds );
	}
}
add_shortcode('ktzagcplugin_text_ask', 'ktzagcplugin_text_ask_shortcode'); /* format [ktzagcplugin_text keyword="" source="" number=""] */

function ktzagcplugin_text_googlenewrss_shortcode( $atts ) {
	extract( shortcode_atts( array(
      'keyword' => '', # keyword
      'max_keyword' => '', # Maximal keyword
      'source' => 'googlenewrss', # source AGC
      'number' => '',  #number show agc
	  'related' => '' #use related by keyword - true/false
      ), $atts ) );	
	if ( $keyword != '' ) {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	$sources = empty($source) ? 'googlenewrss' : $source;
	$numbers = empty($number) ? 4 : $number;
	$relateds = empty($related) ? false : $related;
	if ( $sources == 'googlenewrss' ) {
		return ktzplg_get_googlenewrss_article( $keywords, $max_keywords, $numbers, $relateds );
	}
}
add_shortcode('ktzagcplugin_text_googlenewrss', 'ktzagcplugin_text_googlenewrss_shortcode'); /* format [ktzagcplugin_text keyword="" source="" number=""] */

function ktzagcplugin_text_yahoo_shortcode( $atts ) {
	extract( shortcode_atts( array(
      'keyword' => '', # keyword
      'max_keyword' => '', # Maximal keyword
      'source' => 'yahoo', # source AGC
      'number' => '',  #number show agc
	  'related' => '' #use related by keyword - true/false
      ), $atts ) );	
	if ( $keyword != '' ) {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	$sources = empty($source) ? 'yahoo' : $source;
	$numbers = empty($number) ? 4 : $number;
	$relateds = empty($related) ? false : $related;
	if ( $sources == 'yahoo' ) {
		return ktzplg_get_yahoo_article( $keywords, $max_keywords, $numbers, $relateds );
	}
}
add_shortcode('ktzagcplugin_text_yahoo', 'ktzagcplugin_text_yahoo_shortcode'); /* format [ktzagcplugin_text keyword="" source="" number=""] */
/* 
 * AGC Image Shortcode 
 * @return do_action funct, keyword, numb_image
 * support source: bing and ask
 * Google image grab is confused, it redirect via meta redirect
 */
function ktzagcplugin_image_shortcode( $atts ) {
	extract( shortcode_atts( array(
      'source' => 'bing', # source AGC
      'keyword' => '', # keyword
      'max_keyword' => '', # Maximal keyword
      'number' => '',  #number show agc
	  'related' => '', #use related by keyword - true/false
      'license' => '', # License
      'size' => '' # License
      ), $atts ) );	
	if ( $keyword != '') {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	$sources = empty($source) ? 'bing' : $source;
	$numbers = empty($number) ? 1 : $number;
	$relateds = empty($related) ? false : $related;
	$licenses = empty($license) ? '' : $license;
	$sizes = empty($size) ? '' : $size;
	if ( $sources == 'bing' ) {
		return ktzplg_get_bing_image( $keywords, $max_keywords, $numbers, $relateds, $licenses, $sizes );
	}
}
add_shortcode( 'ktzagcplugin_image', 'ktzagcplugin_image_shortcode' ); /* format [ktzagcplugin_image keyword="" max_keyword="" source="" number="" license=""] */

function ktzagcplugin_image_shortcode_2( $atts ) {
	extract( shortcode_atts( array(
      'source' => 'bing', # source AGC
      'keyword' => '', # keyword
      'max_keyword' => '', # Maximal keyword
      'number' => '',  #number show agc
	  'related' => '', #use related by keyword - true/false
      'license' => '', # License
      'size' => '' # License
      ), $atts ) );	
	if ( $keyword != '') {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	$sources = empty($source) ? 'bing' : $source;
	$numbers = empty($number) ? 1 : $number;
	$relateds = empty($related) ? false : $related;
	$licenses = empty($license) ? '' : $license;
	$sizes = empty($size) ? '' : $size;
	if ( $sources == 'bing' ) {
		return ktzplg_get_bing_image_2( $keywords, $max_keywords, $numbers, $relateds, $licenses, $sizes );
	}
}
add_shortcode( 'ktzagcplugin_image_2', 'ktzagcplugin_image_shortcode_2' );

function ktzagcplugin_google_image_shortcode( $atts ) {
	extract( shortcode_atts( array(
      'source' => 'google', # source AGC
      'keyword' => '', # keyword
      'max_keyword' => '', # Maximal keyword
      'number' => '',  #number show agc
	  'related' => '', #use related by keyword - true/false
      'license' => '' # License
      ), $atts ) );	
	if ( $keyword != '') {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	$sources = empty($source) ? 'google' : $source;
	$numbers = empty($number) ? 1 : $number;
	$relateds = empty($related) ? false : $related;
	$licenses = empty($license) ? '' : $license;
	if ( $sources == 'google' ) {
		return ktzplg_get_google_image( $keywords, $max_keywords, $numbers, $relateds, $licenses );
	}
}
add_shortcode( 'ktzagcplugin_image_google', 'ktzagcplugin_google_image_shortcode' ); 

function ktzagcplugin_yahoo_image_shortcode( $atts ) {
	extract( shortcode_atts( array(
      'source' => 'yahoo', # source AGC
      'keyword' => '', # keyword
      'max_keyword' => '', # Maximal keyword
      'number' => '',  #number show agc
	  'related' => '', #use related by keyword - true/false
      'license' => '' # License
      ), $atts ) );	
	if ( $keyword != '') {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	$sources = empty($source) ? 'yahoo' : $source;
	$numbers = empty($number) ? 1 : $number;
	$relateds = empty($related) ? false : $related;
	$licenses = empty($license) ? '' : $license;
	if ( $sources == 'yahoo' ) {
		return ktzplg_get_yahoo_image( $keywords, $max_keywords, $numbers, $relateds, $licenses );
	}
}
add_shortcode( 'ktzagcplugin_image_yahoo', 'ktzagcplugin_yahoo_image_shortcode' ); 

/* 
 * AGC Video Shortcode 
 * @return do_action funct, keyword, numb_image
 * support source: bing and ask
 */
function ktzagcplugin_video_shortcode( $atts ) {
	extract( shortcode_atts( array(
      'source' => 'bing', # source AGC
      'keyword' => '', # keyword
	  'max_keyword' => '', # Maximal keyword
      'number' => '' # number sentence
      ), $atts ) );
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	$sources = empty($source) ? 'bing' : $source;
	if ( $keyword != '') {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$numbers = empty($number) ? 1 : $number;
	if ( $sources == 'ask' ) {
		return ktzplg_get_ask_video( $keywords, $max_keywords, $numbers );
	} elseif ( $sources == 'bing' ) {
		return ktzplg_get_bing_video( $keywords, $max_keywords, $numbers );
	} elseif ( $sources == 'google' ) {
		return ktzplg_get_google_video( $keywords, $max_keywords, $numbers );
	} else {
		return ktzplg_get_bingss_video( $keywords, $max_keywords, $numbers );
	}
}
add_shortcode('ktzagcplugin_video', 'ktzagcplugin_video_shortcode'); /* format [ktzagcplugin_video keyword="" source="" number=""] */

/* 
 * Spintax functions
 * @return spin_text $text
 */
function ktzplg_replace_spintext($text) {
    $text = ktzplg_spin($text[1]);
    $parts = explode('|', $text);
    return $parts[array_rand($parts)];
}
function ktzplg_spin($text) {
    return preg_replace_callback(
        '/\{(((?>[^\{\}]+)|(?R))*)\}/x',
        'ktzplg_replace_spintext',
        $text
    );
}

/* 
 * Spin text Shortcode Setup
 * @return ktzplg_spin
 */
function ktzplg_render_shortcode( $atts ) {
	extract( shortcode_atts( array(
      'text' => '' # text
      ), $atts ) );
	$texts = empty($text) ? '' : $text;
	return ktzplg_spin( $texts );
}
add_shortcode( 'ktzagcplugin_spin', 'ktzplg_render_shortcode' ); /* format [ktzagcplugin_spin]{phrase 1|phrase 2|phrase 3}[/ktzagcplugin_spin] */

if ( !function_exists('ktzplg_get_google_article') ) {
	/* 
	 * Google keyword
	 */
	function ktzplg_get_keyword( $keyword ) {
		
		$keywords = isset( $keyword ) ? str_replace(' ', '+', $keyword ) : '';
		$output = '';
		$output .= ucfirst($keyword);
		return $output;
	}
	add_action('ktzplg_get_keyword', 'ktzplg_get_keyword', 10, 1);
}

/* 
 * add keyword Shortcode Setup
 * @return ktzplg_get_keyword
 */
function ktzagcplugin_get_title( $atts ) {
	extract( shortcode_atts( array(
      'keyword' => '' # text
      ), $atts ) );
	  
	if ( $keyword != '' ) {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	
	return ktzplg_get_keyword( $keywords );
}
add_shortcode( 'ktzagcplugin_get_title', 'ktzagcplugin_get_title' ); /* format [ktzagcplugin_get_title] */

/* 
 * AGC Video Shortcode 
 * @return do_action funct, keyword, numb_image
 * support source: bing and ask
 */
function ktzagcplugin_cse_shortcode( $atts ) {
	extract( shortcode_atts( array(
      'keyword' => '', # keyword
	  'max_keyword' => '', # Maximal keyword
      'apikey' => '' # api key
      ), $atts ) );
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	if ( $keyword != '') {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$apikeys = empty( $apikey ) ? '' : $apikey;
	
	return ktzplg_get_cse( $keywords, $max_keywords, $apikeys );
}
add_shortcode('ktzagcplugin_cse', 'ktzagcplugin_cse_shortcode'); /* format [ktzagcplugin_cse keyword="" apikey=""] */

function ktzagcplugin_google_sugestion( $atts ) {
	extract( shortcode_atts( array(
      'keyword' => '', # keyword
	  'max_keyword' => '', # Maximal keyword
      // 'apikey' => '' # api key
      ), $atts ) );
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	if ( $keyword != '') {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$apikeys = empty( $apikey ) ? '' : $apikey;
	
	return ktzplg_get_google_sugestion( $keywords, $max_keywords, $apikeys );
}
add_shortcode('ktzagcplugin_google_sugestion', 'ktzagcplugin_google_sugestion');


function ktzagcplugin_google_sugestion_nolink( $atts ) {
	extract( shortcode_atts( array(
      'keyword' => '', # keyword
	  'max_keyword' => '', # Maximal keyword
      // 'apikey' => '' # api key
      ), $atts ) );
	$max_keywords = empty($max_keyword) ? '' : $max_keyword;
	if ( $keyword != '') {
		$keywords = $keyword;
	} elseif ( is_single() || is_page() || is_search() ) {
		$keywords = get_the_title();
	} elseif ( is_category() ) {
		$keywords = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$keywords = single_tag_title( '', false );
	} elseif ( is_404() ) {
		$keywords = ktzplg_get_404_title(); 
	} else {
		$keywords = '';
	}
	$apikeys = empty( $apikey ) ? '' : $apikey;
	
	return ktzplg_get_google_sugestion_nolink( $keywords, $max_keywords, $apikeys );
}
add_shortcode('ktzagcplugin_google_sugestion_nolink', 'ktzagcplugin_google_sugestion_nolink');
