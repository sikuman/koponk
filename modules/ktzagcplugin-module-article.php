<?php
/**
 * The file that defines the article scraper from source
 *
 * @link       https://google.com
 * @since      1.0.0
 *
 * @package    koponk
 * @subpackage koponk/modules
 */

/**
 * Ktzplugin module article
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    koponk
 * @subpackage koponk/modules
 * @author     sikuman
 */
 
/*
 *************************************************************************************
 ********************************* Start Google Article ******************************
 *************************************************************************************
 */
 
if ( !function_exists('ktzplg_get_google_article') ) {
	/* 
	 * Google Article Scrape 
	 */
	function ktzplg_get_google_article( $keyword, $max_keyword = '', $num = 4, $related = false ) {
		
		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';		
		
		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'https://www.google.com/search?q='.$keywords );
		$html = new simple_html_dom();
		
		$html->load($fetch); //Simple HTML now use the result craped by cURL.
		
		$result = array();
		if( $html && is_object($html) )
		{
			foreach($html->find('div.g') as $g)
			{
				/*
				 * each search results are in a list item with a class name 'g'
				 * we are seperating each of the elements within, into an array
				 * Summaries are stored in a span with a classname of 'st'
				 * Title are stored in a h3 with a classname of 'r'
				 */
				$item['title'] = isset($g->find('h3.r', 0)->innertext) ? $g->find('h3.r', 0)->innertext : '';
				$item['description'] = isset($g->find('span.st', 0)->innertext) ? $g->find('span.st', 0)->innertext : '';
				$result[] =  $item;
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			
			/* 
			 * Otherwise it prints out the array structure so that it
			 * is more human readible. You could instead perform a 
			 * foreach loop on the variable $result so that you can 
			 * organize the html output, or insert the data into a database
			 */
			$output = '';
			if($result)
			{
				$i = 0;
				if ( $related == true ) :
					$output .= '<h4 class="ktzplg-title-related">' . __('Related posts to ','ktzagcplugin');
					$output .= $keyword;
					$output .= '</h4>';
					$output .= '<ul class="ktzplg-list-agcplugin">';
					foreach($result as $r)
					{
						$output .= '<li>';
						if ( !empty ($r['title']) ) {
							
							$result_title = ktzplg_filter_badwords( $r['title'] );	
							$result_title = sanitize_title($result_title);
							$result_title = str_replace('-',' ', $result_title);
							$result_title = ktzplg_plugin_clean_character($result_title);
							$result_title = ucwords($result_title);
							$output .= '<h7><a href="'; 
							$output .= ktzplg_permalink( $result_title, $choice = 'default' );
							$output .= '" title="' . __('Permalink to ','ktzagcplugin') . $result_title . '">'; 
							$output .= $result_title;
							$output .= '</a></h7>'; 
							
						}
						if ( !empty ($r['description']) ) {
							
							$result_desc = ktzplg_filter_badwords( $r['description'] );					
							$output .= '<p>' . ucfirst(ktzplg_plugin_clean_character($result_desc)) . '.</p>'; 
							
						}
						$output .= '</li>';
						if(++$i==$num) break;
					}
					$output .= '</ul>';
				else :
					$output .= '<p>';
					foreach($result as $r)
					{
						if ( !empty ($r['description']) ) {
							
							$result_desc = ktzplg_filter_badwords( $r['description'] );
							
							# ktzplg_plugin_clean_character find in _functions.php
							$output .= ucfirst(ktzplg_plugin_clean_character($result_desc)) . '.'; 
							
						}
						if(++$i==$num) break;
					}
					$output .= '</p>';
				endif;
			}

		} 
		return $output;
	} /* End ktzplg_get_google_article */
	add_action('ktzplg_get_google_article', 'ktzplg_get_google_article', 10, 4);
}

/*
 *************************************************************************************
 ********************************* Start Ask.com Article *****************************
 *************************************************************************************
 */

if ( !function_exists('ktzplg_get_ask_article') ) {
	/* 
	 * Ask Article Scrape 
	 */
	function ktzplg_get_ask_article( $keyword, $max_keyword = '', $num = 4, $related = false ) {
		
		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';		
		
		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'https://www.ask.com/web?q='.$keywords );
		$html = new simple_html_dom();
		$html->load($fetch); //Simple HTML now use the result craped by cURL.
		$result = array();
		if( $html && is_object($html) )
		{
			foreach($html->find('div.PartialSearchResults-item') as $g)
			{
				/*
				 * each search results are in a list item with a class name 'g'
				 * we are seperating each of the elements within, into an array
				 * Summaries are stored in a p with a classname of 'web-result-description'
				 * title are stored in a with a classname of 'web-result-title-link'
				 */
				$item['title'] = isset($g->find('a.PartialSearchResults-item-title-link', 0)->innertext) ? $g->find('a.PartialSearchResults-item-title-link', 0)->innertext : '';
				$item['description'] = isset($g->find('p.PartialSearchResults-item-abstract', 0)->innertext) ? $g->find('p.PartialSearchResults-item-abstract', 0)->innertext : '';
				$result[] =  $item;
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			
			/* 
			 * Otherwise it prints out the array structure so that it
			 * is more human readible. You could instead perform a 
			 * foreach loop on the variable $result so that you can 
			 * organize the html output, or insert the data into a database
			 */
			$output = '';
			if($result)
			{
				$i = 0;
				if ( $related == true ) :
					$output .= '<h4 class="ktzplg-title-related">' . __('Related posts to ','ktzagcplugin');
					$output .= $keyword;
					$output .= '</h4>';
					$output .= '<ul class="ktzplg-list-agcplugin">';
					foreach($result as $r)
					{
						$output .= '<li>';
						if ( !empty ($r['title']) ) {
							
							$result_title = ktzplg_filter_badwords( $r['title'] );	
							$result_title = sanitize_title($result_title);
							$result_title = str_replace('-',' ', $result_title);
							$result_title = ktzplg_plugin_clean_character($result_title);
							$result_title = ucwords($result_title);
							$output .= '<h7><a href="'; 
							$output .= ktzplg_permalink( $result_title, $choice = 'default' );
							$output .= '" title="' . __('Permalink to ','ktzagcplugin') . $result_title . '">'; 
							$output .= $result_title;
							$output .= '</a></h7>'; 
							
						}
						if ( !empty ($r['description']) ) {
							
							$result_desc = ktzplg_filter_badwords( $r['description'] );					
							$output .= '<p>' . ucfirst(ktzplg_plugin_clean_character($result_desc)) . '.</p>'; 
							
						}
						$output .= '</li>';
						if(++$i==$num) break;
					}
					$output .= '</ul>';
				else :
					$output .= '<p>';
					foreach($result as $r)
					{
						if ( !empty ($r['description']) ) {
							
							$result = ktzplg_filter_badwords( $r['description'] );
							
							# ktzplg_plugin_clean_character find in _functions.php
							$output .= ucfirst(ktzplg_plugin_clean_character($result)) . '.'; 
							
						}
						if(++$i==$num) break;
					}
					$output .= '</p>';
				endif;
			}
		} 
		return $output;
	} /* End ktzplg_get_ask_article */
	add_action('ktzplg_get_ask_article', 'ktzplg_get_ask_article', 10, 4);
}

/*
 *************************************************************************************
 ********************************* Start bing.com Article ****************************
 *************************************************************************************
 */

if ( !function_exists('ktzplg_get_bing_article') ) {
	/* 
	 * Bing Article Scrape
	 */
	function ktzplg_get_bing_article( $keyword, $max_keyword = '', $num = 4, $related = false ) {
		
		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';		
		
		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'https://www.bing.com/search?q='.$keywords.'&hl=en' );
		
		$html = new simple_html_dom();
		
		$html->load($fetch); //Simple HTML now use the result craped by cURL.

		$result = array();
		if( $html && is_object($html) )
		{
			foreach($html->find('li.b_algo') as $g)
			{
				/*
				 * each search results are in a list item with a class name 'g'
				 * we are seperating each of the elements within, into an array
				 * Summaries are stored in a p
				 * Summaries are stored in h2 a
				 */
				$item['title'] = isset($g->find('h2 a', 0)->innertext) ? $g->find('h2 a', 0)->innertext : '';
				$item['description'] = isset($g->find('p', 0)->innertext) ? $g->find('p', 0)->innertext : '';
				$result[] =  $item;
				// echo "IKIII".var_dump($result);
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			
			/* 
			 * Otherwise it prints out the array structure so that it
			 * is more human readible. You could instead perform a 
			 * foreach loop on the variable $result so that you can 
			 * organize the html output, or insert the data into a database
			 */
			$output = '';
			if(!empty ($result))
			{
				$i = 0;
				if ( $related == true ) :
					$output .= '<h4 class="ktzplg-title-related">' . __('Related posts to ','ktzagcplugin');
					$output .= $keyword;
					$output .= '</h4>';
					$output .= '<ul class="ktzplg-list-agcplugin">';
					foreach($result as $r)
					{
						$output .= '<li>';
						if ( !empty ($r['title']) ) {
							
							$result_title = ktzplg_filter_badwords( $r['title'] );	
							$result_title = sanitize_title($result_title);
							$result_title = str_replace('-',' ', $result_title);
							$result_title = ktzplg_plugin_clean_character($result_title);
							$result_title = ucwords($result_title);
							$output .= '<h7><a href="'; 
							$output .= ktzplg_permalink( $result_title, $choice = 'default' );
							$output .= '" title="' . __('Permalink to ','ktzagcplugin') . $result_title . '">'; 
							$output .= $result_title;
							$output .= '</a></h7>'; 
							
						}
						if ( !empty ($r['description']) ) {
							
							$result_desc = ktzplg_filter_badwords( $r['description'] );					
							$output .= '<p>' . ucfirst(ktzplg_plugin_clean_character($result_desc)) . '.</p>'; 
							
						}
						$output .= '</li>';
						if(++$i==$num) break;
					}
					$output .= '</ul>';
				else :
				$output .= '<p>';
				foreach($result as $r)
				{
					if ( !empty ($r['description']) ) {
						
						# ktzplg_plugin_clean_character find in _functions.php
						$result = ktzplg_filter_badwords( $r['description'] );
						$output .= ucfirst(ktzplg_plugin_clean_character($result)) . '.'; 
						
					}
					if(++$i==$num) break;
				}
				$output .= '</p>';
				endif;
			}
		} 
		return $output;
	} /* End ktzplg_get_bing_article */
	add_action('ktzplg_get_bing_article', 'ktzplg_get_bing_article', 10, 4);
}
if ( !function_exists('ktzplg_get_bing_article_2') ) {
	/* 
	 * Bing Article Scrape
	 */
	function ktzplg_get_bing_article_2( $keyword, $max_keyword = '', $num = 4, $related = false ) {
		
		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';		
		
		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'https://www.bing.com/search?q='.$keywords.'&hl=en'.'&first=11' );
		
		$html = new simple_html_dom();
		
		$html->load($fetch); //Simple HTML now use the result craped by cURL.
		 
		$result = array();
		if( $html && is_object($html) )
		{
			foreach($html->find('li.b_algo') as $g)
			{
				/*
				 * each search results are in a list item with a class name 'g'
				 * we are seperating each of the elements within, into an array
				 * Summaries are stored in a p
				 * Summaries are stored in h2 a
				 */
				$item['title'] = isset($g->find('h2 a', 0)->innertext) ? $g->find('h2 a', 0)->innertext : '';
				$item['description'] = isset($g->find('p', 0)->innertext) ? $g->find('p', 0)->innertext : '';
				$result[] =  $item;
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			
			/* 
			 * Otherwise it prints out the array structure so that it
			 * is more human readible. You could instead perform a 
			 * foreach loop on the variable $result so that you can 
			 * organize the html output, or insert the data into a database
			 */
			$output = '';
			if($result)
			{
				$i = 0;
				if ( $related == true ) :
					$output .= '<h4 class="ktzplg-title-related">' . __('Related posts to ','ktzagcplugin');
					$output .= $keyword;
					$output .= '</h4>';
					$output .= '<ul class="ktzplg-list-agcplugin">';
					foreach($result as $r)
					{
						$output .= '<li>';
						if ( !empty ($r['title']) ) {
							
							$result_title = ktzplg_filter_badwords( $r['title'] );	
							$result_title = sanitize_title($result_title);
							$result_title = str_replace('-',' ', $result_title);
							$result_title = ktzplg_plugin_clean_character($result_title);
							$result_title = ucwords($result_title);
							$output .= '<h7><a href="'; 
							$output .= ktzplg_permalink( $result_title, $choice = 'default' );
							$output .= '" title="' . __('Permalink to ','ktzagcplugin') . $result_title . '">'; 
							$output .= $result_title;
							$output .= '</a></h7>'; 
							
						}
						if ( !empty ($r['description']) ) {
							
							$result_desc = ktzplg_filter_badwords( $r['description'] );					
							$output .= '<p>' . ucfirst(ktzplg_plugin_clean_character($result_desc)) . '.</p>'; 
							
						}
						$output .= '</li>';
						if(++$i==$num) break;
					}
					$output .= '</ul>';
				else :
				$output .= '<p>';
				foreach($result as $r)
				{
					if ( !empty ($r['description']) ) {
						
						# ktzplg_plugin_clean_character find in _functions.php
						$result = ktzplg_filter_badwords( $r['description'] );
						$output .= ucfirst(ktzplg_plugin_clean_character($result)) . '.'; 
						
					}
					if(++$i==$num) break;
				}
				$output .= '</p>';
				endif;
			}
		} 
		return $output;
	} /* End ktzplg_get_bing_article */
	add_action('ktzplg_get_bing_article_2', 'ktzplg_get_bing_article_2', 10, 4);
}
if ( !function_exists('ktzplg_get_bing_article_3') ) {
	/* 
	 * Bing Article Scrape
	 */
	function ktzplg_get_bing_article_3( $keyword, $max_keyword = '', $num = 4, $related = false ) {
		
		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';		
		
		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'https://www.bing.com/search?q='.$keywords.'&hl=en'.'&first=21' );
		
		$html = new simple_html_dom();
		
		$html->load($fetch); //Simple HTML now use the result craped by cURL.
		 
		$result = array();
		if( $html && is_object($html) )
		{
			foreach($html->find('li.b_algo') as $g)
			{
				/*
				 * each search results are in a list item with a class name 'g'
				 * we are seperating each of the elements within, into an array
				 * Summaries are stored in a p
				 * Summaries are stored in h2 a
				 */
				$item['title'] = isset($g->find('h2 a', 0)->innertext) ? $g->find('h2 a', 0)->innertext : '';
				$item['description'] = isset($g->find('p', 0)->innertext) ? $g->find('p', 0)->innertext : '';
				$result[] =  $item;
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			
			/* 
			 * Otherwise it prints out the array structure so that it
			 * is more human readible. You could instead perform a 
			 * foreach loop on the variable $result so that you can 
			 * organize the html output, or insert the data into a database
			 */
			$output = '';
			if($result)
			{
				$i = 0;
				if ( $related == true ) :
					$output .= '<h4 class="ktzplg-title-related">' . __('Related posts to ','ktzagcplugin');
					$output .= $keyword;
					$output .= '</h4>';
					$output .= '<ul class="ktzplg-list-agcplugin">';
					foreach($result as $r)
					{
						$output .= '<li>';
						if ( !empty ($r['title']) ) {
							
							$result_title = ktzplg_filter_badwords( $r['title'] );	
							$result_title = sanitize_title($result_title);
							$result_title = str_replace('-',' ', $result_title);
							$result_title = ktzplg_plugin_clean_character($result_title);
							$result_title = ucwords($result_title);
							$output .= '<h7><a href="'; 
							$output .= ktzplg_permalink( $result_title, $choice = 'default' );
							$output .= '" title="' . __('Permalink to ','ktzagcplugin') . $result_title . '">'; 
							$output .= $result_title;
							$output .= '</a></h7>'; 
							
						}
						if ( !empty ($r['description']) ) {
							
							$result_desc = ktzplg_filter_badwords( $r['description'] );					
							$output .= '<p>' . ucfirst(ktzplg_plugin_clean_character($result_desc)) . '.</p>'; 
							
						}
						$output .= '</li>';
						if(++$i==$num) break;
					}
					$output .= '</ul>';
				else :
				$output .= '<p>';
				foreach($result as $r)
				{
					if ( !empty ($r['description']) ) {
						
						# ktzplg_plugin_clean_character find in _functions.php
						$result = ktzplg_filter_badwords( $r['description'] );
						$output .= ucfirst(ktzplg_plugin_clean_character($result)) . '.'; 
						
					}
					if(++$i==$num) break;
				}
				$output .= '</p>';
				endif;
			}
		} 
		return $output;
	} /* End ktzplg_get_bing_article */
	add_action('ktzplg_get_bing_article_3', 'ktzplg_get_bing_article_3', 10, 4);
}
if ( !function_exists('ktzplg_get_bing_article_4') ) {
	/* 
	 * Bing Article Scrape
	 */
	function ktzplg_get_bing_article_4( $keyword, $max_keyword = '', $num = 4, $related = false ) {
		
		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';		
		
		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'https://www.bing.com/search?q='.$keywords.'&hl=en'.'&first=31' );
		
		$html = new simple_html_dom();
		
		$html->load($fetch); //Simple HTML now use the result craped by cURL.
		 
		$result = array();
		if( $html && is_object($html) )
		{
			foreach($html->find('li.b_algo') as $g)
			{
				/*
				 * each search results are in a list item with a class name 'g'
				 * we are seperating each of the elements within, into an array
				 * Summaries are stored in a p
				 * Summaries are stored in h2 a
				 */
				$item['title'] = isset($g->find('h2 a', 0)->innertext) ? $g->find('h2 a', 0)->innertext : '';
				$item['description'] = isset($g->find('p', 0)->innertext) ? $g->find('p', 0)->innertext : '';
				$result[] =  $item;
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			
			/* 
			 * Otherwise it prints out the array structure so that it
			 * is more human readible. You could instead perform a 
			 * foreach loop on the variable $result so that you can 
			 * organize the html output, or insert the data into a database
			 */
			$output = '';
			if($result)
			{
				$i = 0;
				if ( $related == true ) :
					$output .= '<h4 class="ktzplg-title-related">' . __('Related posts to ','ktzagcplugin');
					$output .= $keyword;
					$output .= '</h4>';
					$output .= '<ul class="ktzplg-list-agcplugin">';
					foreach($result as $r)
					{
						$output .= '<li>';
						if ( !empty ($r['title']) ) {
							
							$result_title = ktzplg_filter_badwords( $r['title'] );	
							$result_title = sanitize_title($result_title);
							$result_title = str_replace('-',' ', $result_title);
							$result_title = ktzplg_plugin_clean_character($result_title);
							$result_title = ucwords($result_title);
							$output .= '<h7><a href="'; 
							$output .= ktzplg_permalink( $result_title, $choice = 'default' );
							$output .= '" title="' . __('Permalink to ','ktzagcplugin') . $result_title . '">'; 
							$output .= $result_title;
							$output .= '</a></h7>'; 
							
						}
						if ( !empty ($r['description']) ) {
							
							$result_desc = ktzplg_filter_badwords( $r['description'] );					
							$output .= '<p>' . ucfirst(ktzplg_plugin_clean_character($result_desc)) . '.</p>'; 
							
						}
						$output .= '</li>';
						if(++$i==$num) break;
					}
					$output .= '</ul>';
				else :
				$output .= '<p>';
				foreach($result as $r)
				{
					if ( !empty ($r['description']) ) {
						
						# ktzplg_plugin_clean_character find in _functions.php
						$result = ktzplg_filter_badwords( $r['description'] );
						$output .= ucfirst(ktzplg_plugin_clean_character($result)) . '.'; 
						
					}
					if(++$i==$num) break;
				}
				$output .= '</p>';
				endif;
			}
		} 
		return $output;
	} /* End ktzplg_get_bing_article */
	add_action('ktzplg_get_bing_article_4', 'ktzplg_get_bing_article_4', 10, 4);
}
if ( !function_exists('ktzplg_get_bing_article_5') ) {
	/* 
	 * Bing Article Scrape
	 */
	function ktzplg_get_bing_article_5( $keyword, $max_keyword = '', $num = 4, $related = false ) {
		
		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';		
		
		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'https://www.bing.com/search?q='.$keywords.'&hl=en'.'&first=41' );
		
		$html = new simple_html_dom();
		
		$html->load($fetch); //Simple HTML now use the result craped by cURL.
		 
		$result = array();
		if( $html && is_object($html) )
		{
			foreach($html->find('li.b_algo') as $g)
			{
				/*
				 * each search results are in a list item with a class name 'g'
				 * we are seperating each of the elements within, into an array
				 * Summaries are stored in a p
				 * Summaries are stored in h2 a
				 */
				$item['title'] = isset($g->find('h2 a', 0)->innertext) ? $g->find('h2 a', 0)->innertext : '';
				$item['description'] = isset($g->find('p', 0)->innertext) ? $g->find('p', 0)->innertext : '';
				$result[] =  $item;
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			
			/* 
			 * Otherwise it prints out the array structure so that it
			 * is more human readible. You could instead perform a 
			 * foreach loop on the variable $result so that you can 
			 * organize the html output, or insert the data into a database
			 */
			$output = '';
			if($result)
			{
				$i = 0;
				if ( $related == true ) :
					$output .= '<h4 class="ktzplg-title-related">' . __('Related posts to ','ktzagcplugin');
					$output .= $keyword;
					$output .= '</h4>';
					$output .= '<ul class="ktzplg-list-agcplugin">';
					foreach($result as $r)
					{
						$output .= '<li>';
						if ( !empty ($r['title']) ) {
							
							$result_title = ktzplg_filter_badwords( $r['title'] );	
							$result_title = sanitize_title($result_title);
							$result_title = str_replace('-',' ', $result_title);
							$result_title = ktzplg_plugin_clean_character($result_title);
							$result_title = ucwords($result_title);
							$output .= '<h7><a href="'; 
							$output .= ktzplg_permalink( $result_title, $choice = 'default' );
							$output .= '" title="' . __('Permalink to ','ktzagcplugin') . $result_title . '">'; 
							$output .= $result_title;
							$output .= '</a></h7>'; 
							
						}
						if ( !empty ($r['description']) ) {
							
							$result_desc = ktzplg_filter_badwords( $r['description'] );					
							$output .= '<p>' . ucfirst(ktzplg_plugin_clean_character($result_desc)) . '.</p>'; 
							
						}
						$output .= '</li>';
						if(++$i==$num) break;
					}
					$output .= '</ul>';
				else :
				$output .= '<p>';
				foreach($result as $r)
				{
					if ( !empty ($r['description']) ) {
						
						# ktzplg_plugin_clean_character find in _functions.php
						$result = ktzplg_filter_badwords( $r['description'] );
						$output .= ucfirst(ktzplg_plugin_clean_character($result)) . '.'; 
						
					}
					if(++$i==$num) break;
				}
				$output .= '</p>';
				endif;
			}
		} 
		return $output;
	} /* End ktzplg_get_bing_article */
	add_action('ktzplg_get_bing_article_5', 'ktzplg_get_bing_article_5', 10, 4);
}
/*
 *************************************************************************************
 ********************** Start bing.com Article with format RSS ***********************
 *************************************************************************************
 */
if ( !function_exists('ktzplg_get_googlenewrss_article') ) {
	/* 
	 * Google Rss Article Scrape
	 * This Function From Google RSS in theme agcsuper
	 */
	function ktzplg_get_googlenewrss_article( $keyword, $max_keyword = '', $num = 4, $related = false ) {
		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';		
		
		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		
		/*
		 * This is Google XML where the picture come from. :D
		 */
		$xmlgoogle = array('https://news.google.com/news/feeds?hl=en&gl=ca&q='.$keywords.'&um=1&ie=UTF-8&output=rss');
		if ($xmlgoogle) {
			foreach ($xmlgoogle as $xmlgoogle) {

				$rss = fetch_feed($xmlgoogle);
			
				if ( ! is_wp_error( $rss ) ) : # Checks that the object is created correctly
					// Figure out how many total items there are, but limit it to $num. 
					$maxitems = $rss->get_item_quantity( $num ); 
					// Build an array of all the items, starting with element $startfrom (first element).
					$google_result = $rss->get_items( 0, $maxitems );
				endif;
				
				$output = '';
				if ( ! is_wp_error( $rss ) ) :
					if ( $maxitems != 0  ) {
						if ( $related == true ) :
							$output .= '<h4 class="ktzplg-title-related">' . __('Related posts to ','ktzagcplugin');
							$output .= $keyword;
							$output .= '</h4>';
							$output .= '<ul class="ktzplg-list-agcplugin">';
							foreach ($google_result as $google_results)
							{
								$output .= '<li>';
								$result_title = ktzplg_filter_badwords( $google_results->get_title() );	
								$result_title = sanitize_title($result_title);
								$result_title = str_replace('-',' ', $result_title);
								$result_title = ktzplg_plugin_clean_character($result_title);
								$result_title = ucwords($result_title);
								$output .= '<h7><a href="'; 
								$output .= ktzplg_permalink( $result_title, $choice = 'default' );
								$output .= '" title="' . __('Permalink to ','ktzagcplugin') . $result_title . '">'; 
								$output .= $result_title;
								$output .= '</a></h7>'; 
									
								/* 
								 * ktzplg_plugin_clean_character find in _functions.php
								 */
								$result_desc = ktzplg_filter_badwords( $google_results->get_description() );
								$output .= '<p>' . ucfirst(ktzplg_plugin_clean_character($result_desc)) . '.</p>';
								$output .= '</li>';
							}
							$output .= '</ul>';
						else :
							$output .= '<p>';
							foreach ($google_result as $google_results) {
								/* 
								 * ktzplg_plugin_clean_character find in _functions.php
								 */
								$result = ktzplg_filter_badwords( $google_results->get_description() );
								$output .= ucfirst(ktzplg_plugin_clean_character($result)) . '.';
							}
							$output .= '</p>';
						endif;
					}
				endif;
			}
		}
		return $output;
	}/* End ktzplg_get_googlenewrss_article */
	add_action('ktzplg_get_googlenewrss_article', 'ktzplg_get_googlenewrss_article', 10, 4);
}

/*
 *************************************************************************************
 ********************************* Start yahoo.com Article *****************************
 *************************************************************************************
 */

if ( !function_exists('ktzplg_get_yahoo_article') ) {
	/* 
	 * Ask Article Scrape 
	 */
	function ktzplg_get_yahoo_article( $keyword, $max_keyword = '', $num = 4, $related = false ) {
		
		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';		
		
		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'https://id.search.yahoo.com/search?p='.$keywords );
	
		$html = new simple_html_dom();
		
		$html->load($fetch); //Simple HTML now use the result craped by cURL.
		 
		$result = array();
		if( $html && is_object($html) )
		{
			foreach($html->find('div#web li') as $g)
			{
				$item['title'] = isset($g->find('h3.title', 0)->innertext) ? $g->find('h3.title', 0)->innertext : '';
				$item['description'] = isset($g->find('p', 0)->innertext) ? $g->find('p', 0)->innertext : '';
				$result[] =  $item;
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			
			/* 
			 * Otherwise it prints out the array structure so that it
			 * is more human readible. You could instead perform a 
			 * foreach loop on the variable $result so that you can 
			 * organize the html output, or insert the data into a database
			 */
			$output = '';
			if($result)
			{
				$i = 0;
				if ( $related == true ) :
					$output .= '<h4 class="ktzplg-title-related">' . __('Related posts to ','ktzagcplugin');
					$output .= $keyword;
					$output .= '</h4>';
					$output .= '<ul class="ktzplg-list-agcplugin">';
					foreach($result as $r)
					{
						$output .= '<li>';
						if ( !empty ($r['title']) ) {
							
							$result_title = ktzplg_filter_badwords( $r['title'] );	
							$result_title = sanitize_title($result_title);
							$result_title = str_replace('-',' ', $result_title);
							$result_title = ktzplg_plugin_clean_character($result_title);
							$result_title = ucwords($result_title);
							$output .= '<h7><a href="'; 
							$output .= ktzplg_permalink( $result_title, $choice = 'default' );
							$output .= '" title="' . __('Permalink to ','ktzagcplugin') . $result_title . '">'; 
							$output .= $result_title;
							$output .= '</a></h7>'; 
							
						}
						if ( !empty ($r['description']) ) {
							
							$result_desc = ktzplg_filter_badwords( $r['description'] );					
							$output .= '<p>' . ucfirst(ktzplg_plugin_clean_character($result_desc)) . '.</p>'; 
							
						}
						$output .= '</li>';
						if(++$i==$num) break;
					}
					$output .= '</ul>';
				else :
					$output .= '<p>';
					foreach($result as $r)
					{
						if ( !empty ($r['description']) ) {
							
							$result = ktzplg_filter_badwords( $r['description'] );
							
							# ktzplg_plugin_clean_character find in _functions.php
							$output .= ucfirst(ktzplg_plugin_clean_character($result)) . '.'; 
							
						}
						if(++$i==$num) break;
					}
					$output .= '</p>';
				endif;
			}
		} 
		return $output;
	} /* End ktzplg_get_yahoo_article */
	add_action('ktzplg_get_yahoo_article', 'ktzplg_get_yahoo_article', 10, 4);
}