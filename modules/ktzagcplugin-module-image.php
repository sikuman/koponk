<?php
/**
 * The file that defines the image scraper
 *
 * @link       http://google.com
 * @since      1.0.0
 *
 * @package    koponk
 * @subpackage koponk/modules
 */

/**
 * Ktzplugin module image
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    koponk
 * @subpackage koponk/modules
 * @author     sikuman
 */
 
/*
 *************************************************************************************
 ************************************* Start Bing Image ******************************
 *************************************************************************************
 */
 
if ( !function_exists('ktzplg_get_bing_image') ) {
	/* 
	 * bing Image Scrape 
	 */
	function ktzplg_get_bing_image( $keyword, $max_keyword = '', $num = 1, $related = false, $license = '',$size='') {

		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';		
		
		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		
		# Add license filter to query
		if ( $license == 'free_to_use' ) {
			$qlicense = '&qft=+filterui:license-L2_L3_L4_L5_L6_L7';
		} elseif ( $license == 'free_to_use_commercial' ) {
			$qlicense = '&qft=+filterui:license-L2_L3_L4';
		} elseif ( $license == 'free_to_modify_use' ) {
			$qlicense = '&qft=+filterui:license-L2_L3_L5_L6';
		} elseif ( $license == 'free_to_modify_use_commercial' ) {
			$qlicense = '&qft=+filterui:license-L2_L3';
		} elseif ( $license == 'public_domain' ) {
			$qlicense = '&qft=+filterui:license-L1';
		} else{
			$qlicense = '&qft=+filterui:license-L2_L3_L4_L5_L6_L7';
		}
		if ($size == 'large'){
			$size = '+filterui:imagesize-large&FORM=IRFLTR';
		}elseif($size == 'medium'){
			$size = '+filterui:imagesize-large&FORM=IRFLTR';
		}
		else{
			$size = '+filterui:imagesize-large&FORM=IRFLTR';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'https://www.bing.com/images/search?q='.$keywords.$qlicense.$size );
		$html = new simple_html_dom();
		
		$html->load( $fetch ); //Simple HTML now use the result craped by cURL.
		 
		$result = array();
		if( $html && is_object($html) )
		{
			foreach($html->find('div[class="dg_b"] div[class="imgpt"]') as $gm)
			{
				/*
				 * each search results are in a list item with a class name '$gm'
				 * we are seperating each of the elements within, into an array
				 */
			
				# Find element a with m attribute where json code can find in div.dg_u
				$get_m_attr = $gm->find('a', 0)->m;
				
				# Fixed json code
				$get_m_attr = ktzplg_fix_json( stripslashes ( html_entity_decode( $get_m_attr ) ) );
				
				# Decode json
				$get_json_m = json_decode( $get_m_attr,true );
				
				# Get link with key surl in json code
			    $item['link'] = $get_json_m['purl'];
				# Get imgurl with key imgurl in json code
				if (!empty ($get_json_m['turl'])){
				$item['imgsrc'] = $get_json_m['turl'];
				}else {
				$item['imgsrc'] = $get_json_m['murl'];
				}
				# Get attribute t1 in a that is title image in bing search
				$item['title'] = $get_json_m['t'];
				$result[] =  $item;
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			/* 
			 * Otherwise it prints out the array structure so that it
			 * is more human readible. You could instead perform a 
			 * foreach loop on the variable $result so that you can 
			 * organize the html output, or insert the data into a database
			 */
			$output = '';
			if($result)
			{
				$i = 0;
				if ( $related == true ) :
					$output .= '<h3 class="ktzplg-title-related">' . __('Related images to ','ktzagcplugin');
					$output .= $keyword;
					$output .= '</h3>';
					$output .= '<ul class="ktzplg-list-image-agcplugin">';
					foreach($result as $r)
					{
						$output .= '<li>';
						if ( !empty ($r['imgsrc']) ) {
							$result_title = ktzplg_filter_badwords( $r['title'] );
							$result_title = str_replace ('result','',$result_title);
							$result_title = sanitize_title($result_title);
							$result_title = str_replace('-',' ', $result_title);
							$result_title = ktzplg_plugin_clean_character($result_title);
							$result_title = ucwords($result_title);
							$output .= '<a href="'; 
							$output .= ktzplg_permalink( $result_title, $choice = 'default' );
							$output .= '" title="' . __('ktzagcplugin') . $result_title . '">'; 
							$output .= '<img src="';
							if ( !empty ($r['imgsrc']) ) {
								
						#code start here	
							$gambar = $r['imgsrc'];
								$output .= $gambar; 
						#code end here
							}
							$output .= '"'; 
							if ( !empty ($r['title']) ) {
								$output .= ' alt="' . $result_title . '" title="' . $result_title . '"';
							}
							$output .= ' />';
							$output .= '</a>'; 
							
						}
						$output .= '</li>';
						if(++$i==$num) break;
					}
					$output .= '</ul>';
				else :
					foreach($result as $r)
					{
						$output .= '<div class="wp-caption aligncenter">';
							$output .= '<img src="';
							if ( !empty ( $r['imgsrc'] ) ) {
								
						#code start here		
								$gambar = $r['imgsrc'];

								$output .= $gambar; 
						#code end here		
								
								
							}
							$output .= '"'; 
							if ( !empty ($r['title']) ) {
								$result_title = ktzplg_filter_badwords( $r['title'] );
								$result_title = str_replace ('result','',$result_title);
								$result_title = sanitize_title($result_title);
								$result_title = str_replace('-',' ', $result_title);
								$result_title = ktzplg_plugin_clean_character($result_title);
								$result_title = ucwords($result_title);
								$output .= ' alt="' . $result_title . '" title="' . $result_title . '"';
							}
							$output .= ' />';
							if ( !empty ($r['title']) ) {
								$result_title = ktzplg_filter_badwords( $r['title'] );
								$result_title = str_replace ('result','',$result_title);
								$result_title = sanitize_title($result_title);
								$result_title = str_replace('-',' ', $result_title);
								$result_title = ktzplg_plugin_clean_character($result_title);
								$result_title = ucwords($result_title);
								$output .= '<p class="wp-caption-text">'. $result_title .'</p>'.'</center>';
							}
						$output .= '</div>';
						if(++$i==$num) break;
					}
				endif;
			}
		}
		return $output;
	} /* End ktzplg_get_bing_image */
	add_action('ktzplg_get_bing_image', 'ktzplg_get_bing_image', 10, 5);
}


if ( !function_exists('ktzplg_get_bing_image_2') ) {
	/* 
	 * bing Image Scrape 
	 */
	function ktzplg_get_bing_image_2( $keyword, $max_keyword = '', $num = 1, $related = false, $license = '',$size='' ) {

		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';		
		
		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		
		# Add license filter to query
		if ( $license == 'free_to_use' ) {
			$qlicense = '&qft=+filterui:license-L2_L3_L4_L5_L6_L7';
		} elseif ( $license == 'free_to_use_commercial' ) {
			$qlicense = '&qft=+filterui:license-L2_L3_L4';
		} elseif ( $license == 'free_to_modify_use' ) {
			$qlicense = '&qft=+filterui:license-L2_L3_L5_L6';
		} elseif ( $license == 'free_to_modify_use_commercial' ) {
			$qlicense = '&qft=+filterui:license-L2_L3';
		} elseif ( $license == 'public_domain' ) {
			$qlicense = '&qft=+filterui:license-L1';
		} else{
			$qlicense = '&qft=+filterui:license-L2_L3_L4_L5_L6_L7';
		}
		if ($size == 'large'){
			$size = '+filterui:imagesize-large&FORM=IRFLTR';
		}elseif($size == 'medium'){
			$size = '+filterui:imagesize-large&FORM=IRFLTR';
		}
		else{
			$size = '+filterui:imagesize-large&FORM=IRFLTR';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'https://www.bing.com/images/search?q='.$keywords.$qlicense.$size );
		 
		$html = new simple_html_dom();
		
		$html->load( $fetch ); //Simple HTML now use the result craped by cURL.
		 
		$result = array();
		if( $html && is_object($html) )
		{
			foreach($html->find('div[class="dg_b"] div[class="imgpt"]') as $gm)
			{
				/*
				 * each search results are in a list item with a class name '$gm'
				 * we are seperating each of the elements within, into an array
				 */
			
				# Find element a with m attribute where json code can find in div.dg_u
				$get_m_attr = $gm->find('a', 0)->m;
				
				# Fixed json code
				$get_m_attr = ktzplg_fix_json( stripslashes ( html_entity_decode( $get_m_attr ) ) );
				
				# Decode json
				$get_json_m = json_decode( $get_m_attr,true );
				
				# Get link with key surl in json code
			    $item['link'] = $get_json_m['purl'];
				# Get imgurl with key imgurl in json code
				if (!empty ($get_json_m['turl'])){
				$item['imgsrc'] = $get_json_m['turl'];
				}else {
				$item['imgsrc'] = $get_json_m['murl'];
				}
				# Get attribute t1 in a that is title image in bing search
				$item['title'] = $get_json_m['t'];
				$result[] =  $item;
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			
			/* 
			 * Otherwise it prints out the array structure so that it
			 * is more human readible. You could instead perform a 
			 * foreach loop on the variable $result so that you can 
			 * organize the html output, or insert the data into a database
			 */
			$output = '';
			if($result)
			{
				$i = 0;
				if ( $related == true ) :
					$output .= '<h3 class="ktzplg-title-related">' . __('Related images to ','ktzagcplugin');
					$output .= $keyword;
					$output .= '</h3>';
					$output .= '<ul class="ktzplg-list-image-agcplugin">';
					foreach($result as $r)
					{
						$output .= '<li>';
						if ( !empty ($r['imgsrc']) ) {
							$result_title = ktzplg_filter_badwords( $r['title'] );
							$result_title = str_replace ('result','',$result_title);
							$result_title = sanitize_title($result_title);
							$result_title = str_replace('-',' ', $result_title);
							$result_title = ktzplg_plugin_clean_character($result_title);
							$result_title = ucwords($result_title);
							$output .= '<a href="'; 
							$output .= ktzplg_permalink( $result_title, $choice = 'default' );
							$output .= '" title="' . __('ktzagcplugin') . $result_title . '">'; 
							$output .= '<img src="';
							if ( !empty ($r['imgsrc']) ) {
								
						#code start here	
							$gambar = $r['imgsrc'];
								$output .= $gambar; 
						#code end here
							}
							$output .= '"'; 
							if ( !empty ($r['title']) ) {
								$output .= ' alt="' . $result_title . '" title="' . $result_title . '"';
							}
							$output .= ' />';
							$output .= '</a>'; 
							
						}
						$output .= '</li>';
						if(++$i==$num) break;
					}
					$output .= '</ul>';
				else :
					foreach($result as $r)
					{
						$output .= '<div class="wp-caption aligncenter">';
							$output .= '<img src="';
							if ( !empty ( $r['imgsrc'] ) ) {
								
						#code start here		
							$gambar = $r['imgsrc'];
								$output .= $gambar; 
						#code end here		
								
								
							}
							$output .= '"'; 
							if ( !empty ($r['title']) ) {
								$result_title = ktzplg_filter_badwords( $r['title'] );
								$result_title = str_replace ('result','',$result_title);
								$result_title = sanitize_title($result_title);
								$result_title = str_replace('-',' ', $result_title);
								$result_title = ktzplg_plugin_clean_character($result_title);
								$result_title = ucwords($result_title);
								$output .= ' alt="' . $result_title . '" title="' . $result_title . '"';
							}
							$output .= ' />';
							if ( !empty ($r['title']) ) {
								$result_title = ktzplg_filter_badwords( $r['title'] );
								$result_title = str_replace ('result','',$result_title);
								$result_title = sanitize_title($result_title);
								$result_title = str_replace('-',' ', $result_title);
								$result_title = ktzplg_plugin_clean_character($result_title);
								$result_title = ucwords($result_title);
								$output .= '<p class="wp-caption-text">'. $result_title .'</p>'.'</center>';
							}
						$output .= '</div>';
						if(++$i==$num) break;
					}
				endif;
			}
		}
		return $output;
	} /* End ktzplg_get_bing_image */
	add_action('ktzplg_get_bing_image_2', 'ktzplg_get_bing_image_2', 10, 5);
}

/*
 *************************************************************************************
 ********************************** Start yahoo.com Image ******************************
 *************************************************************************************
 */

if ( !function_exists('ktzplg_get_yahoo_image') ) {
	/* 
	 * yahoo.com Image Scrape 
	 */
	function ktzplg_get_yahoo_image( $keyword, $max_keyword = '', $num = 1, $related = false, $license = '' ) {

		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(' ', '+', $keyword ) : '';
		
		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		
		// Add license filter to query
		if ( $license == 'free_to_use' ) {
			$qlicense = '&imgl=fsu';
		} elseif ( $license == 'free_to_use_commercial' ) {
			$qlicense = '&imgl=fsuc';
		} elseif ( $license == 'free_to_modify_use' ) {
			$qlicense = '&imgl=fmsu';
		} elseif ( $license == 'free_to_modify_use_commercial' ) {
			$qlicense = '&imgl=fmsuc';
		} elseif ( $license == 'public_domain' ) {
			$qlicense = '&imgl=pd';
		} else{
			$qlicense = '&imgl=fsu';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'https://images.search.yahoo.com/search/images?p='.$keywords.$license );
		
		$html = new simple_html_dom();
		
		$html->load($fetch); //Simple HTML now use the result craped by cURL.
		 
		$result = array();
		if( $html && is_object($html) )
		{
			foreach($html->find('ul[id="sres"] li') as $gm)
			{
				
				# Find element a
				$string = $gm->find('img', 1)->src;
				#set imgurl get from bing
				$cleanUrl= substr($string, 0, strpos($string, "&P="));
				$imgurl = $cleanUrl;
				#get image title
				$findtitle = $gm -> find('a',0)->href;
				parse_str( $findtitle , $output );
				$title     = isset( $output['tt'] ) ? $output['tt'] : ''; 
				
				# Get imgurl with key imgurl in json code
				$item['imgsrc'] = ktzplg_plugin_addhttp(urldecode($imgurl));
				
				# Get attribute t1 in a that is title image in yandex search
				$item['title'] = $title;
				
				$result[] =  $item;
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			
			/* 
			 * Otherwise it prints out the array structure so that it
			 * is more human readible. You could instead perform a 
			 * foreach loop on the variable $result so that you can 
			 * organize the html output, or insert the data into a database
			 */
			$output = '';
			if($result)
			{
				$i = 0;
				if ( $related == true ) :
					$output .= '<h3 class="ktzplg-title-related">' . __('Related images to ','ktzagcplugin');
					$output .= $keyword;
					$output .= '</h3>';
					$output .= '<ul class="ktzplg-list-image-agcplugin">';
					foreach($result as $r)
					{
						$output .= '<li>';
						if ( !empty ($r['imgsrc']) ) {
							
						#code start here	
								$output .= $r['imgsrc']; 
						#code end here
							
							$result_title = ktzplg_filter_badwords( $r['title'] );	
							$result_title = sanitize_title($result_title);
							$result_title = str_replace('-',' ', $result_title);
							$result_title = ktzplg_plugin_clean_character($result_title);
							$result_title = ucwords($result_title);
							$output .= '<a href="'; 
							$output .= ktzplg_permalink( $result_title, $choice = 'default' );
							$output .= '" title="' . __('Permalink to ','ktzagcplugin') . $result_title . '">'; 
							$output .= '<img src="';
							if ( !empty ($r['imgsrc']) ) {
								
								$output .= $r['imgsrc']; 
								
							}
							$output .= '"'; 
							if ( !empty ($r['title']) ) {
								$output .= ' alt="' . $result_title . '" title="' . $result_title . '"';
							}
							$output .= ' />';
							$output .= '</a>'; 
							
						}
						$output .= '</li>';
						if(++$i==$num) break;
					}
					$output .= '</ul>';
				else :
					foreach($result as $r)
					{
						$output .= '<div class="wp-caption aligncenter">';
							$output .= '<img src="';
							if ( !empty ($r['imgsrc']) ) {
								
						#code start here		
								$output .= $r['imgsrc']; 
						#code end here		
							
							}
							$output .= '"'; 
							if ( !empty ($r['title']) ) {
								$result_title = ktzplg_filter_badwords( $r['title'] );	
								$result_title = sanitize_title($result_title);
								$result_title = str_replace('-',' ', $result_title);
								$result_title = ktzplg_plugin_clean_character($result_title);
								$result_title = ucwords($result_title);
								$output .= ' alt="' . $result_title . '" title="' . $result_title . '"';
							}
							$output .= ' />';
							if ( !empty ($r['title']) ) {
								$result_title = ktzplg_filter_badwords( $r['title'] );	
								$result_title = sanitize_title($result_title);
								$result_title = str_replace('-',' ', $result_title);
								$result_title = ktzplg_plugin_clean_character($result_title);
								$result_title = ucwords($result_title);
								$output .= '<p class="wp-caption-text">'. $result_title .'</p>';
							}
						$output .= '</div>';
						if(++$i==$num) break;
					}
				endif;
			}
		
		}
		return $output;
	} /* End ktzplg_get_yahoo_image */
	add_action('ktzplg_get_yahoo_image', 'ktzplg_get_yahoo_image', 10, 5);
}

/*
 *************************************************************************************
 ************************************* Start Google Image ******************************
 *************************************************************************************
*/
 
if ( !function_exists('ktzplg_get_google_image') ) {
	# 
	# Google Image Scrape 
	#
	function ktzplg_get_google_image( $keyword, $max_keyword = '', $num = 1, $related = false, $license = '' ) {

		// Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';

		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		
		// Add license filter to query
		if ( $license == 'free_to_use' ) {
			$qlicense = '&tbs=sur:f';
		} elseif ( $license == 'free_to_use_commercial' ) {
			$qlicense = '&tbs=sur:fc';
		} elseif ( $license == 'free_to_modify_use' ) {
			$qlicense = '&tbs=sur:fm';
		} elseif ( $license == 'free_to_modify_use_commercial' ) {
			$qlicense = '&tbs=sur:fmc';
		} else {
			$qlicense = '';
		}
		 
		# 
		# @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		# ktzplg_fetch_agc find in _functions.php
		#
		// $fetch = ktzplg_fetch_agc( 'http://www.bing.com/images/search?q='.$keywords.$qlicense );
		$fetch = ktzplg_fetch_agc( 'https://www.google.com/search?q='.$keywords.$license.'&newwindow=1&tbm=isch&cad=h' );
		 
		$html = new simple_html_dom();
		
		$html->load( $fetch ); //Simple HTML now use the result craped by cURL.
		 
		$result = array();
		if( $html && is_object($html) )
		{
			// foreach($html->find('div[class="dg_b"] div[class="dg_u"]') as $gm)
			foreach( $html->find('div[class="rg_meta"]') as $gm )
			{
				$gm = strip_tags($gm);
				
				$data = json_decode($gm, true);
				
				# Decode json
				
				# Get link with key surl in json code
			    $item['link'] = $data['ru'];
				
				# Get imgurl with key imgurl in json code
				$item['imgsrc'] = $data['ou'];
				
				# Get attribute t1 in a that is title image in bing search
				$item['title'] = $data['s'];
				$result[] =  $item;
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			
			# 
			# Otherwise it prints out the array structure so that it
			# is more human readible. You could instead perform a 
			# foreach loop on the variable $result so that you can 
			# organize the html output, or insert the data into a database
			#
			$output = '';
			if($result)
			{
				$i = 0;
				if ( $related == true ) :
					$output .= '<h3 class="ktzplg-title-related">' . __('Related images to ','ktzagcplugin');
					$output .= $keyword;
					$output .= '</h3>';
					$output .= '<ul class="ktzplg-list-image-agcplugin">';
					foreach($result as $r)
					{
						$output .= '<li>';
						if ( !empty ($r['imgsrc']) ) {
							
						//code start here		
								$gambar = $r['imgsrc'];
								$to_replace = array ('http://','https://');
								$replace_link = str_replace($to_replace,"http://i0.wp.com/",$gambar."?fit=200,200");
								if (strpos($gambar, 'blogspot')){
								$output .= $gambar; 
								}else{
								$output .= $replace_link; 
								}
						//code end here		
							
							$result_title = ktzplg_filter_badwords( $r['title'] );	
							$result_title = sanitize_title($result_title);
							$result_title = str_replace('-',' ', $result_title);
							$result_title = ktzplg_plugin_clean_character($result_title);
							$result_title = ucwords($result_title);
							$output .= '<a href="';
							#cek empty title
							if (!empty ($r['title'])){
							$output .= ktzplg_permalink( $result_title, $choice = 'default' );
							$output .= '" title="' . __('Permalink to ','ktzagcplugin') . $result_title . '">'; 
								}else {
							$output .= ktzplg_permalink( $keyword, $choice = 'default' );
							$output .= '" title="' . __('Permalink to ','ktzagcplugin') . $keyword . '">'; 
								} 
							$output .= '<img src="';
							if ( !empty ($r['imgsrc']) ) {
								
								$output .= $r['imgsrc']; 
							
							}
							$output .= '"'; 
							if ( !empty ($r['title']) ) {
								$output .= ' alt="' . $result_title . '" title="' . $result_title . '"';
							}
							$output .= ' />';
							$output .= '</a>'; 
							
						}
						$output .= '</li>';
						if(++$i==$num) break;
					}
					$output .= '</ul>';
				else :
					foreach($result as $r)
					{
						$output .= '<div class="wp-caption aligncenter">';
							$output .= '<img width="640" height="542" src="';
							if ( !empty ( $r['imgsrc'] ) ) {
						//code start here		
								$gambar = $r['imgsrc'];
								$to_replace = array ('http://','https://');
								$replace_link = str_replace($to_replace,"http://i0.wp.com/",$gambar);
								if (strpos($gambar, 'blogspot')){
								$output .= $gambar; 
								}else{
								$output .= $replace_link; 
								}
						//code end here		
							}
							$output .= '"'; 
							if ( !empty ($r['title']) ) {
								$result_title = ktzplg_filter_badwords( $r['title'] );	
								$result_title = sanitize_title($result_title);
								$result_title = str_replace('-',' ', $result_title);
								$result_title = ktzplg_plugin_clean_character($result_title);
								$result_title = ucwords($result_title);
								$output .= ' alt="' . $result_title . '" title="' . $result_title . '"';
							}
							$output .= ' />';
							if ( !empty ($r['title']) ) {
								$result_title = ktzplg_filter_badwords( $r['title'] );	
								$result_title = sanitize_title($result_title);
								$result_title = str_replace('-',' ', $result_title);
								$result_title = ktzplg_plugin_clean_character($result_title);
								$result_title = ucwords($result_title);
								$output .= '<p class="wp-caption-text">'. $result_title .'</p>';
							}
						$output .= '</div>';
						if(++$i==$num) break;
					}
				endif;
			}


		}
		return $output;
	} # End ktzplg_get_google_image
	add_action('ktzplg_get_google_image', 'ktzplg_get_google_image', 10, 5);
}
if ( !function_exists('ktzplg_get_yandex_image') ) {
	/* 
	 * bing Image Scrape 
	 */
	function ktzplg_get_yandex_image( $keyword, $max_keyword = '', $num = 1, $related = false, $license = '' ) {

		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';		
		
		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		
		// Add license filter to query
		if ( $license == 'free_to_use' ) {
			$qlicense = '&qft=+filterui:license-L2_L3_L4_L5_L6_L7';
		} elseif ( $license == 'free_to_use_commercial' ) {
			$qlicense = '&qft=+filterui:license-L2_L3_L4';
		} elseif ( $license == 'free_to_modify_use' ) {
			$qlicense = '&qft=+filterui:license-L2_L3_L5_L6';
		} elseif ( $license == 'free_to_modify_use_commercial' ) {
			$qlicense = '&qft=+filterui:license-L2_L3';
		} elseif ( $license == 'public_domain' ) {
			$qlicense = '&qft=+filterui:license-L1';
		} else{
			$qlicense = '';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'https://yandex.com/images/search?text='.$keywords.$qlicense );
		 
		$html = new simple_html_dom();
		
		$html->load( $fetch ); //Simple HTML now use the result craped by cURL.
		 
		$result = array();
		if( $html && is_object($html) )
		{
			foreach($html->find('div[class="dg_b"] div[class="imgpt"]') as $gm)
			{
				/*
				 * each search results are in a list item with a class name '$gm'
				 * we are seperating each of the elements within, into an array
				 */
			
				# Find element a with m attribute where json code can find in div.dg_u
				$get_m_attr = $gm->find('a', 0)->m;
				
				# Fixed json code
				$get_m_attr = ktzplg_fix_json( stripslashes ( html_entity_decode( $get_m_attr ) ) );
				
				# Decode json
				$get_json_m = json_decode( $get_m_attr,true );
				
				# Get link with key surl in json code
			    $item['link'] = $get_json_m['purl'];
				# Get imgurl with key imgurl in json code
				$item['imgsrc'] = $get_json_m['murl'];
				
				# Get attribute t1 in a that is title image in bing search
				$item['title'] = $gm->find('img', 0)->alt;
				$result[] =  $item;
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			
			/* 
			 * Otherwise it prints out the array structure so that it
			 * is more human readible. You could instead perform a 
			 * foreach loop on the variable $result so that you can 
			 * organize the html output, or insert the data into a database
			 */
			$output = '';
			if($result)
			{
				$i = 0;
				if ( $related == true ) :
					$output .= '<h3 class="ktzplg-title-related">' . __('Related images to ','ktzagcplugin');
					$output .= $keyword;
					$output .= '</h3>';
					$output .= '<ul class="ktzplg-list-image-agcplugin">';
					foreach($result as $r)
					{
						$output .= '<li>';
						if ( !empty ($r['imgsrc']) ) {
							$result_title = ktzplg_filter_badwords( $r['title'] );	
							$result_title = sanitize_title($result_title);
							$result_title = str_replace('-',' ', $result_title);
							$result_title = ktzplg_plugin_clean_character($result_title);
							$result_title = ucwords($result_title);
							$output .= '<a href="'; 
							$output .= ktzplg_permalink( $result_title, $choice = 'default' );
							$output .= '" title="' . __('ktzagcplugin') . $result_title . '">'; 
							$output .= '<img src="';
							if ( !empty ($r['imgsrc']) ) {
								
						#code start here	
							$gambar = $r['imgsrc'];
								$output .= $gambar; 
						#code end here
							}
							$output .= '"'; 
							if ( !empty ($r['title']) ) {
								$output .= ' alt="' . $result_title . '" title="' . $result_title . '"';
							}
							$output .= ' />';
							$output .= '</a>'; 
							
						}
						$output .= '</li>';
						if(++$i==$num) break;
					}
					$output .= '</ul>';
				else :
					foreach($result as $r)
					{
						$output .= '<div class="wp-caption aligncenter">';
							$output .= '<img src="';
							if ( !empty ( $r['imgsrc'] ) ) {
								
						#code start here		
								$gambar = $r['imgsrc'];

								$output .= $gambar; 
						#code end here		
								
								
							}
							$output .= '"'; 
							if ( !empty ($r['title']) ) {
								$result_title = ktzplg_filter_badwords( $r['title'] );	
								$result_title = sanitize_title($result_title);
								$result_title = str_replace('-',' ', $result_title);
								$result_title = ktzplg_plugin_clean_character($result_title);
								$result_title = ucwords($result_title);
								$output .= ' alt="' . $result_title . '" title="' . $result_title . '"';
							}
							$output .= ' />';
							if ( !empty ($r['title']) ) {
								$result_title = ktzplg_filter_badwords( $r['title'] );	
								$result_title = sanitize_title($result_title);
								$result_title = str_replace('-',' ', $result_title);
								$result_title = ktzplg_plugin_clean_character($result_title);
								$result_title = ucwords($result_title);
								$output .= '<p class="wp-caption-text">'. $result_title .'</p>'.'</center>';
							}
						$output .= '</div>';
						if(++$i==$num) break;
					}
				endif;
			}
		}
		return $output;
	} /* End ktzplg_get_bing_image */
	add_action('ktzplg_get_yandex_image', 'ktzplg_get_yandex_image', 10, 5);
}
