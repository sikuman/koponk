<?php
 error_reporting(0);
/**
 * The file that defines the article scraper from source
 *
 * @link       http://google.com
 * @since      1.0.0
 *
 * @package    koponk
 * @subpackage koponk/modules
 */

/**
 * Ktzplugin module article
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    koponk
 * @subpackage koponk/modules
 * @author     sikuman
 */
 
/*
 *************************************************************************************
 ********************************* Start Google Article ******************************
 *************************************************************************************
 */
 
if ( !function_exists('ktzplg_get_google_sugestion') ) {
	/* 
	 * Google Article Scrape 
	 */
	function ktzplg_get_google_sugestion( $keyword, $max_keyword = '') {
		
		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';		
		
		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'https://google.com/complete/search?output=toolbar&q='.$keywords );
		$fetch_bing = ktzplg_fetch_agc( "https://api.bing.com/osjson.aspx?query=".$keywords);
		
		$kiwot = json_decode ( $fetch_bing, 1 );
;
		if(isset($kiwot[0])) {
            foreach($kiwot[1]as $keys) {
                $keyword_bing .= $keys;
			}
        }
		$xml = simplexml_load_string(utf8_encode($fetch));
		foreach ($xml -> CompleteSuggestion as $sugg){
			$keyword_goog .= $sugg->suggestion['data'].',';
			
		}
		$array_google = explode (",",$keyword_goog);
		$kw = array_merge(array_diff($array_google,$kiwot[1]),array_diff($kiwot[1],$array_google));
		foreach ($kw as $kw_jadi){
			$output .= '<a href="'. ktzplg_permalink( $kw_jadi, $choice = 'default' ) .'">';
			$output .= $kw_jadi;
			$output .= '</a>';
			$output .= '&nbsp;';
		}
		return $output;
	} 
	add_action('ktzplg_get_google_sugestion', 'ktzplg_get_google_sugestion', 10);
}

if ( !function_exists('ktzplg_get_google_sugestion_nolink') ) {
	/* 
	 * Google Article Scrape 
	 */
	function ktzplg_get_google_sugestion_nolink( $keyword, $max_keyword = '') {
		
		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';		
		
		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'https://google.com/complete/search?output=toolbar&q='.$keywords );
		$fetch_bing = ktzplg_fetch_agc( "https://api.bing.com/osjson.aspx?query=".$keywords);
		
		$kiwot = json_decode ( $fetch_bing, 1 );
;
		if(isset($kiwot[0])) {
            foreach($kiwot[1]as $keys) {
                $keyword_bing .= $keys;
			}
        }
		$xml = simplexml_load_string(utf8_encode($fetch));
		foreach ($xml -> CompleteSuggestion as $sugg){
			$keyword_goog .= $sugg->suggestion['data'].',';
			
		}
		$array_google = explode (",",$keyword_goog);
		$kw = array_merge(array_diff($array_google,$kiwot[1]),array_diff($kiwot[1],$array_google));
		foreach ($kw as $kw_jadi){
			$output .=$kw_jadi.',';
		}
		return $output;
	} 
	add_action('ktzplg_get_google_sugestion_nolink', 'ktzplg_get_google_sugestion_nolink', 10);
}
