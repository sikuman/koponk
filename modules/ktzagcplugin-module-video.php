<?php
/**
 * The file that defines the video scraper
 *
 * @link       http://google.com
 * @since      1.0.0
 *
 * @package    koponk
 * @subpackage koponk/modules
 */

/**
 * Ktzplugin module video
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    koponk
 * @subpackage koponk/modules
 * @author     sikuman
 */
 
/**
 * get youtube video ID from URL
 *
 * @param string $youtube
 * @return string Youtube video id or FALSE if none found. 
 */
function ktzplg_get_youtube_id_from_url( $youtube ) {
	$regex = '~(?:http|https|)(?::\/\/|)(?:www.|)(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/ytscreeningroom\?v=|\/feeds\/api\/videos\/|\/user\S*[^\w\-\s]|\S*[^\w\-\s]))([\w\-]{11})[a-z0-9;:@#?&%=+\/\$_.-]*~i';
 	$id = preg_replace( $regex, '$1', $youtube );
	return $id;
}
 
/*
 *************************************************************************************
 ************************************* Start Bing video ******************************
 *************************************************************************************
 */
 
if ( !function_exists('ktzplg_get_bingss_video') ) {
	/* 
	 * bing video Scrape 
	 */
	function ktzplg_get_bingss_video( $keyword, $max_keyword = '', $num = 1 ) {	

		// Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';

		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		
		/*
		 * This is BING XML where the picture come from. :D
		 */
		$xmlbing = array('http://www.bing.com/search?q='.$keywords.'+site:youtube.com&go=&form=QBLH&filt=all&format=rss');
		if ($xmlbing) {
			foreach ($xmlbing as $xmlbing) {

				$rss = fetch_feed($xmlbing);
			
				if ( ! is_wp_error( $rss ) ) : # Checks that the object is created correctly
					// Figure out how many total items there are, but limit it to $num. 
					$maxitems = $rss->get_item_quantity( $num ); 
					// Build an array of all the items, starting with element $startfrom (first element).
					$bing_result = $rss->get_items( 0, $maxitems );
				endif;
				$output = '';
				if ( ! is_wp_error( $rss ) ) :
					if ( $maxitems != 0  ) {

						foreach ($bing_result as $bing_results) {
							// get youtube id with function, please see above function
							$youtube_id = ktzplg_get_youtube_id_from_url( trim( $bing_results->get_link() ) );
							$output .= '<p>';
							/* 
							 * ktzplg_plugin_clean_character find in _functions.php
							 * ucfirst(strtolower( this function doc: http://php.net/ucfirst
							 */
							$output .= '<div class="ktzplg-responsive-video"><iframe height="352" width="625" src="https://www.youtube.com/embed/'.$youtube_id.'" frameborder="0" allowfullscreen></iframe></div>';
							$output .= '</p>';
						}
					}
				endif;
			}
		}
		return $output;
	} /* End ktzplg_get_bingss_video */
	add_action('ktzplg_get_bingss_video', 'ktzplg_get_bingss_video', 10, 3);
}

/*
 *************************************************************************************
 ************************************* Start aSk video ******************************
 *************************************************************************************
 */
 
if ( !function_exists('ktzplg_get_ask_video') ) {
	/* 
	 * Ask Video Scrape 
	 */
	function ktzplg_get_ask_video( $keyword, $max_keyword = '', $num = 1 ) {
		
		// Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';

		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		
		//Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(' ', '+', $keyword ) : '';
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'http://www.ask.com/youtube?q='.$keywords );
		
		$html = new simple_html_dom();
		
		$html->load($fetch); //Simple HTML now use the result craped by cURL.
		 
		$result = array();
		if( $html && is_object($html) )
		{
			foreach($html->find('div.video-result') as $g)
			{
				/*
				 * each search results are in a list item with a class name 'g'
				 * we are seperating each of the elements within, into an array
				 * Summaries are stored in a p with a classname of 'web-result-description'
				 */
				$item['videourl'] = isset($g->find('.video-result-thumbnail-link', 0)->href) ? $g->find('.video-result-thumbnail-link', 0)->href : '';
				$result[] =  $item;
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			
			/* 
			 * Otherwise it prints out the array structure so that it
			 * is more human readible. You could instead perform a 
			 * foreach loop on the variable $result so that you can 
			 * organize the html output, or insert the data into a database
			 */
			$output = '';
			if($result)
			{
				$i = 0;
				$output .= '<p>';
				foreach($result as $r)
				{
					if ( !empty ($r['videourl']) ) {
						
						// get youtube id with function, please see above function
						$youtube_id = ktzplg_get_youtube_id_from_url( $r['videourl'] );
						
						# ktzplg_plugin_clean_character find in _functions.php
						$output .= '<div class="ktzplg-responsive-video"><iframe height="352" width="625" src="https://www.youtube.com/embed/'.$youtube_id.'" frameborder="0" allowfullscreen></iframe></div>';
						
					}
					if(++$i==$num) break;
				}
				$output .= '</p>';
			}
		} 
		return $output;
	} /* End ktzplg_get_ask_video */
	add_action('ktzplg_get_ask_video', 'ktzplg_get_ask_video', 10, 3);
}

/*
 *************************************************************************************
 ************************************* Start bing video ******************************
 *************************************************************************************
 */
 
if ( !function_exists('ktzplg_get_bing_video') ) {
	/* 
	 * Ask Video Scrape 
	 */
	function ktzplg_get_bing_video( $keyword, $max_keyword = '', $num = 1 ) {
		
		// Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';

		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php
		 */
		$fetch = ktzplg_fetch_agc( 'http://www.bing.com/videos/search?q='.$keywords.'&qft=+filterui:msite-youtube.com' );
		
		$html = new simple_html_dom();
		
		$html->load($fetch); //Simple HTML now use the result craped by cURL.
		 
		$result = array();
		if( $html && is_object($html) )
		{
			foreach($html->find('div[class="dg_b"] div[class="dg_u"]') as $gm)
			{
				# get atribute data-eventpayload in div with class sa_wrapper
				$get_attr = $gm->find('div[class="sa_wrapper"]', 0)->attr[ 'data-eventpayload' ];
				
				# Fixed json code
				$get_attr = ktzplg_fix_json( stripslashes ( html_entity_decode( $get_attr ) ) );
				
				# Decode json
				$json_it = json_decode( $get_attr,true );
				
				# Multi level json parsing for video data-eventpayload='{"collectionType":4,"query":"modern home","mid":"A126A1D4EA384C27049FA126A1D4EA384C27049F","murl":"https://www.youtube.com/watch?v=4Wee4LASqvE","purl":"https://www.youtube.com/watch?v=4Wee4LASqvE"}' gotcha babe. :D
				$item['videourl'] = $json_it['purl'];
				
				$result[] =  $item;
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			
			/* 
			 * Otherwise it prints out the array structure so that it
			 * is more human readible. You could instead perform a 
			 * foreach loop on the variable $result so that you can 
			 * organize the html output, or insert the data into a database
			 */
			$output = '';
			if($result)
			{
				$i = 0;
				$output .= '<p>';
				foreach($result as $r)
				{
					if ( !empty ($r['videourl']) ) {
						
						// get youtube id with function, please see above function
						$youtube_id = ktzplg_get_youtube_id_from_url( $r['videourl'] );
						
						# ktzplg_plugin_clean_character find in _functions.php
						$output .= '<div class="ktzplg-responsive-video"><iframe height="352" width="625" src="https://www.youtube.com/embed/'.$youtube_id.'" frameborder="0" allowfullscreen></iframe></div>';
					}
					if(++$i==$num) break;
				}
				$output .= '</p>';
			}
		} 
		return $output;
	} /* End ktzplg_get_bing_video */
	add_action('ktzplg_get_bing_video', 'ktzplg_get_bing_video', 10, 3);
}

/*
 *************************************************************************************
 ************************************* Start bing video ******************************
 *************************************************************************************
 */
 
if ( !function_exists('ktzplg_get_google_video') ) {
	/* 
	 * Ask Video Scrape 
	 */
	function ktzplg_get_google_video( $keyword, $max_keyword = '', $num = 1 ) {
		
		// Set query if any passed
		$keywords = isset( $keyword ) ? str_replace(array(' ', '-'), '+', $keyword ) : '';

		// Add maximal number keyword in search query
		if ( $max_keyword != '' ) {
			$max_keywords = (int)$max_keyword;
			$value = explode( ' ', $keyword );
			$keywords_5_first = isset( $keyword ) ? implode( ' ', array_splice( $value, 0, $max_keywords ) ) : '';
			$keywords = isset( $keywords_5_first ) ? str_replace(array(' ', '-'), '+', $keywords_5_first ) : '';
		}
		 
		/* 
		 * @str_get_html Simple HTML DOM and get url via ktzplg_fetch_agc ( curl and fopen )
		 * ktzplg_fetch_agc find in _functions.php source from youtube
		 */
		$fetch = ktzplg_fetch_agc( 'https://www.google.com/search?q='.$keywords.'&tbs=srcf:H4sIAAAAAAAAACWJQQrAIAwEf-Ol4J-iLFqIpKxR8Pct6WFgmLmOLV8FudpIbqqF5h2cEboNTD8K_1l_1Y4KEifLKxpUqI3xv6EecFr4h-W1IAAAA&tbm=vid' );
		
		$html = new simple_html_dom();
		
		$html->load($fetch); //Simple HTML now use the result craped by cURL.
		 
		$result = array();
		if( $html && is_object($html) )
		{
			foreach($html->find('div[id="ires"] div[class="g"]') as $gm)
			{
				$item['videourl'] = isset($gm->find('div[style="margin-left:125px"] cite', 0)->innertext) ? $gm->find('div[style="margin-left:125px"] cite', 0)->innertext : '';
				
				$result[] =  $item;
			}
		
			// Clean up memory
			$html->clear();
			unset($html);
			
			/* 
			 * Otherwise it prints out the array structure so that it
			 * is more human readible. You could instead perform a 
			 * foreach loop on the variable $result so that you can 
			 * organize the html output, or insert the data into a database
			 */
			$output = '';
			if($result)
			{
				$i = 0;
				$output .= '<p>';
				foreach($result as $r)
				{
					if ( !empty ($r['videourl']) ) {
						
						// get youtube id with function, please see above function
						$youtube_id = ktzplg_get_youtube_id_from_url( $r['videourl'] );
						
						# ktzplg_plugin_clean_character find in _functions.php
						$output .= '<div class="ktzplg-responsive-video"><iframe height="352" width="625" src="https://www.youtube.com/embed/'.$youtube_id.'" frameborder="0" allowfullscreen></iframe></div>';
					}
					if(++$i==$num) break;
				}
				$output .= '</p>';
			}
		} 
		return $output;
	} /* End ktzplg_get_google_video */
	add_action('ktzplg_get_google_video', 'ktzplg_get_google_video', 10, 3);
}